//
//  UIFunctionButton.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/31/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBorderButton.h"


IB_DESIGNABLE
@interface UIFunctionButton : UIBorderButton

@property (nonatomic) IBInspectable int buttonFunction;

@end


