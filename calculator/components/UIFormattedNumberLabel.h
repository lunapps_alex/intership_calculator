//
//  UIFormattedNumberLabel.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/31/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFormattedNumberLabel : UILabel
-(void) setText:(NSString *)text;
@end
