//
//  UIFormattedNumberLabel.m
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/31/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import "UIFormattedNumberLabel.h"

@implementation UIFormattedNumberLabel
-(void) setText:(NSString *)text {
    //adding " " each 4 characters, e.g. 111222223 -> 1112 2222 3
    
    NSMutableString *formattedString = [NSMutableString string];
    [formattedString setString:text];
    NSString *mainPart = [[text componentsSeparatedByString:@"."] objectAtIndex:0];
    
    for (int p = 0; p < [mainPart length]; p++) {
        if (p%3 == 0 && p != 0) {
            [formattedString insertString:@" " atIndex:[mainPart length] - p];
        }
    }
    [super setText:formattedString];
}
@end
