//
//  UIBorderButton.m
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/31/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import "UIBorderButton.h"

@implementation UIBorderButton


-(void) commonInit {
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor colorWithRed: 0.5 green: 0.5 blue:0.5 alpha: 1.0].CGColor;
}

- (id)initWithFrame:(CGRect)aRect {
    if ((self = [super initWithFrame:aRect])) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder {
    if ((self = [super initWithCoder:coder])) {
        [self commonInit];
    }
    return self;
}

@end
