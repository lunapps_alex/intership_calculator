//
//  UIBorderButton.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/31/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIBorderButton : UIButton

- (id)initWithFrame:(CGRect)aRect;
- (id)initWithCoder:(NSCoder*)coder;

@end


