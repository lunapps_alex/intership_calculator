//
//  UINumberButton.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBorderButton.h"


IB_DESIGNABLE
@interface UINumberButton : UIBorderButton

@property (nonatomic) IBInspectable int buttonValue;

@end

