//
//  AppDelegate.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

