//
//  ViewController.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model/Calculator.h"

@interface CalculatorViewController : UIViewController

@property (strong, nonatomic, getter=getsimpleCalculator) Calculator* simpleCalculator;

-(Calculator*) getsimpleCalculator;

@end