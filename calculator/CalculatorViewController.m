//
//  ViewController.m
//  calculator
//
//  Created by Alexandr Shyshatskiy on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import "CalculatorViewController.h"
#import "UINumberButton.h"
#import "UIFunctionButton.h"
#import "Model/Calculator.h"

@interface CalculatorViewController ()

@property (weak, nonatomic) IBOutlet UILabel *calculatorLabel;

@end

@implementation CalculatorViewController

-(Calculator*) getsimpleCalculator{
    if(!_simpleCalculator) {
        _simpleCalculator = [[Calculator alloc]init];
    }
    
    return _simpleCalculator;
}

- (IBAction)functionButtonTouch:(UIFunctionButton *)sender {
    NSLog(@"functionButtonTouch: %i", sender.buttonFunction);
    
    self.simpleCalculator.operationNumber = sender.buttonFunction;
    self.calculatorLabel.text = [NSString stringWithFormat:@"%G", self.simpleCalculator.value];
}


- (IBAction)numberButtonTouch:(UINumberButton *)sender {
    NSLog(@"numberButtonTouch: %i", sender.buttonValue);

    if(self.simpleCalculator.isDoubleTail){
        self.simpleCalculator.value *= pow(10, self.simpleCalculator.doubleTailLength + 1);
        self.simpleCalculator.value += sender.buttonValue;
        self.simpleCalculator.value /= pow(10, self.simpleCalculator.doubleTailLength + 1);
        self.simpleCalculator.doubleTailLength++;
    } else{
        self.simpleCalculator.value *= 10;
        self.simpleCalculator.value += sender.buttonValue;
    }
    
    self.calculatorLabel.text = [NSString stringWithFormat:@"%G", self.simpleCalculator.value];
}


@end
