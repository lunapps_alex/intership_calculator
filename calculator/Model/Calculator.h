//
//  Calculator.h
//  calculator
//
//  Created by Buzko Yaroslav on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Operations.h"


@interface Calculator : NSObject


@property (assign, nonatomic, readonly) double firstValue;
@property (assign, nonatomic, readonly) double secondValue;
@property (assign, nonatomic, setter=setOperation:, getter=getOperation) OperationType operationNumber;
@property (assign, nonatomic, setter=setValue:, getter=getValue) double value;

@property (assign, nonatomic, readonly) BOOL isDoubleTail;
@property (assign, nonatomic, readwrite) int doubleTailLength;

-(void)calculate;
-(void)setValue:(double)value;
-(double)getValue;
-(void)setOperation:(OperationType)operationNumber;
-(void)resetValue;
-(void)setDefault;



@end
