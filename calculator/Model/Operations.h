//
//  Operations.h
//  calculator
//
//  Created by Alexandr Shyshatskiy on 4/1/16.
//  Copyright © 2016 lunapps. All rights reserved.
//
#import "Foundation/Foundation.h"

#ifndef Operations_h
#define Operations_h

typedef NS_ENUM(NSUInteger, OperationType) {
    OperationSubstrain = 1,
    OperationMultiply = 2,
    OperationDivide = 3,
    OperationPercentage = 4,
    OperationDefault = 5,
    OperationReset = 6,
    OperationOpposite = 7,
    OperationAdd = 8,
    OperationDot = 9,
};

#endif /* Operations_h */
