//
//  Calculator.m
//  calculator
//
//  Created by Buzko Yaroslav on 3/30/16.
//  Copyright © 2016 lunapps. All rights reserved.
//

#import "Calculator.h"
@interface Calculator()

@property (assign, nonatomic, readwrite) double firstValue;
@property (assign, nonatomic, readwrite) double secondValue;
@property (assign, nonatomic)BOOL firstIsChosen;
@property (assign, nonatomic, readwrite) BOOL isDoubleTail;

@end

@implementation Calculator

-(instancetype)init {
    self = [super init];
    
    if (self) {
        [self setDefault];
    }
    
    return self;
}

@synthesize operationNumber = _operationNumber;

-(void)setDefault {
    _operationNumber = OperationDefault;
    self.isDoubleTail = NO;
    self.doubleTailLength = 0;
    self.firstValue = 0;
    self.secondValue = 0;
    self.firstIsChosen = YES;
}

-(void)calculate {
    self.firstIsChosen = !self.firstIsChosen;
    if (!self.firstIsChosen) {
        [self calculateFirst];
    }
}

-(void)setOperation:(OperationType)operationNumber {
    if(_operationNumber && operationNumber == OperationDefault){
        //case of '=' touched
        [self calculateFirst];
        self.firstIsChosen = true;
        self.secondValue = 0;
    } else if(operationNumber == OperationOpposite){
        //case of +-, change value, save old operation
        self.value *= -1;
        return;
    } else if(operationNumber == OperationPercentage){
        //case of %, change value, save old operation
        self.value /= 100;
        return;
    } else if(operationNumber == OperationDot){
        //case of ',', save old operation
        self.isDoubleTail = YES;
        return;
    }
    
    _operationNumber = operationNumber;
    [self calculateFirst];
}

-(OperationType)getOperation {
    return _operationNumber;
}

-(void)calculateFirst {
    if(!self.firstIsChosen){
        switch (self.operationNumber) {
            case OperationAdd:
                _firstValue = [self addValues];
                break;
            case OperationSubstrain:
                _firstValue = [self substrainValues];
                break;
            case OperationMultiply:
                _firstValue = [self multiplyValues];
                break;
            case OperationDivide:
                _firstValue = [self divideValues];
                break;
            default:
                break;
        }
    }
    
    switch (self.operationNumber) {
        case OperationAdd:
        case OperationSubstrain:
        case OperationMultiply:
        case OperationDivide:
            self.firstIsChosen = false;
            self.isDoubleTail = NO;
            self.doubleTailLength = 0;
            break;
        case OperationReset:
            [self setDefault];
            break;
        default:
            break;
    }
    
}

-(void)setValue:(double)value{
    if (self.firstIsChosen) {
        self.firstValue = value;
    } else {
        self.secondValue = value;
    }
}
-(double)getValue {
    return self.firstIsChosen ? self.firstValue : self.secondValue;
}

-(double)addValues {
    return [Calculator addValue:self.firstValue toValue:self.secondValue];
}
-(double)substrainValues {
    return [Calculator substrainValue:self.firstValue toValue:self.secondValue];
}
-(double)multiplyValues {
    return [Calculator multiplyValue:self.firstValue toValue:self.secondValue];
}
-(double)divideValues {
    return [Calculator divideValue:self.firstValue byValue:self.secondValue];
}
-(double)percetageValue {
    return [Calculator divideValue:self.firstValue byValue:100];
}
-(void)oppositeValue {
    if (self.firstIsChosen) {
        self.firstValue = - self.firstValue;
    } else {
        self.secondValue = - self.secondValue;
    }
}
-(void)resetValue {
    if (self.firstIsChosen) {
        _firstValue = 0;
    } else {
        self.secondValue = 0;
    }
}

+(double)addValue:(double)firstValue
          toValue:(double)secondValue {
    return firstValue + secondValue;
}
+(double)substrainValue:(double)firstValue
                toValue:(double)secondValue{
    return firstValue - secondValue;
}
+(double)multiplyValue:(double)firstValue
               toValue:(double)secondValue {
    return firstValue * secondValue;
}
+(double)divideValue:(double)firstValue
             byValue:(double)secondValue {
    if (secondValue == 0) {
        return NAN;
    } else {
        return firstValue / secondValue;
    }
}

@end
